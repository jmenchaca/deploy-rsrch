{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "CoreOS on EC2: http://coreos.com/docs/running-coreos/cloud-providers/ec2/",
  "Mappings" : {
      "RegionMap" : {

          "eu-central-1" : {
              "AMI" : "ami-9501c8fa"
          },

          "ap-northeast-1" : {
              "AMI" : "ami-885f19ef"
          },

          "us-gov-west-1" : {
              "AMI" : "ami-12c67c73"
          },

          "ap-northeast-2" : {
              "AMI" : "ami-d65889b8"
          },

          "ca-central-1" : {
              "AMI" : "ami-c8c67bac"
          },

          "ap-south-1" : {
              "AMI" : "ami-7e641511"
          },

          "sa-east-1" : {
              "AMI" : "ami-3e5d3952"
          },

          "ap-southeast-2" : {
              "AMI" : "ami-d92422ba"
          },

          "ap-southeast-1" : {
              "AMI" : "ami-14cc7877"
          },

          "us-east-1" : {
              "AMI" : "ami-fd6c94eb"
          },

          "us-east-2" : {
              "AMI" : "ami-72032617"
          },

          "us-west-2" : {
              "AMI" : "ami-4c49f22c"
          },

          "us-west-1" : {
              "AMI" : "ami-b6bae7d6"
          },

          "eu-west-1" : {
              "AMI" : "ami-ac8fd4ca"
          },

          "eu-west-2" : {
              "AMI" : "ami-054c5961"
          }

      }
  },
  "Parameters": {
    "InstanceType" : {
      "Description" : "EC2 HVM instance type (m3.medium, etc).",
      "Type" : "String",
      "Default" : "m3.medium",
      "ConstraintDescription" : "Must be a valid EC2 HVM instance type."
    },
    "ClusterSize": {
      "Default": "3",
      "MinValue": "3",
      "MaxValue": "12",
      "Description": "Number of nodes in cluster (3-12).",
      "Type": "Number"
    },
    "DiscoveryURL": {
      "Description": "An unique etcd cluster discovery URL. Grab a new token from https://discovery.etcd.io/new?size=<your cluster size>",
      "Type": "String"
    },
    "AdvertisedIPAddress": {
      "Description": "Use 'private' if your etcd cluster is within one region or 'public' if it spans regions or cloud providers.",
      "Default": "private",
      "AllowedValues": ["private", "public"],
      "Type": "String"
    },
    "AllowSSHFrom": {
      "Description": "The net block (CIDR) that SSH is available to.",
      "Default": "0.0.0.0/0",
      "Type": "String"
    },
    "KeyPair" : {
      "Description" : "The name of an EC2 Key Pair to allow SSH access to the instance.",
      "Type" : "String"
    }
  },
  "Resources": {
    "CoreOSSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "CoreOS SecurityGroup",
        "SecurityGroupIngress": [
          {"IpProtocol": "tcp", "FromPort": "22", "ToPort": "22", "CidrIp": {"Ref": "AllowSSHFrom"}}
        ]
      }
    },
    "Ingress4001": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupName": {"Ref": "CoreOSSecurityGroup"}, "IpProtocol": "tcp", "FromPort": "4001", "ToPort": "4001", "SourceSecurityGroupId": {
          "Fn::GetAtt" : [ "CoreOSSecurityGroup", "GroupId" ]
        }
      }
    },
    "Ingress2379": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupName": {"Ref": "CoreOSSecurityGroup"}, "IpProtocol": "tcp", "FromPort": "2379", "ToPort": "2379", "SourceSecurityGroupId": {
          "Fn::GetAtt" : [ "CoreOSSecurityGroup", "GroupId" ]
        }
      }
    },
    "Ingress2380": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupName": {"Ref": "CoreOSSecurityGroup"}, "IpProtocol": "tcp", "FromPort": "2380", "ToPort": "2380", "SourceSecurityGroupId": {
          "Fn::GetAtt" : [ "CoreOSSecurityGroup", "GroupId" ]
        }
      }
    },
    "CoreOSServerAutoScale": {
      "Type": "AWS::AutoScaling::AutoScalingGroup",
      "Properties": {
        "AvailabilityZones": {"Fn::GetAZs": ""},
        "LaunchConfigurationName": {"Ref": "CoreOSServerLaunchConfig"},
        "MinSize": "3",
        "MaxSize": "12",
        "DesiredCapacity": {"Ref": "ClusterSize"},
        "Tags": [
            {"Key": "Name", "Value": { "Ref" : "AWS::StackName" }, "PropagateAtLaunch": true}
        ]
      }
    },
    "CoreOSServerLaunchConfig": {
      "Type": "AWS::AutoScaling::LaunchConfiguration",
      "Properties": {
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "InstanceType": {"Ref": "InstanceType"},
        "KeyName": {"Ref": "KeyPair"},
        "SecurityGroups": [{"Ref": "CoreOSSecurityGroup"}],
        "UserData" : { "Fn::Base64":
          { "Fn::Join": [ "", [
            "#cloud-config\n\n",
            "coreos:\n",
            "  etcd2:\n",
            "    discovery: ", { "Ref": "DiscoveryURL" }, "\n",
            "    advertise-client-urls: http://$", { "Ref": "AdvertisedIPAddress" }, "_ipv4:2379\n",
            "    initial-advertise-peer-urls: http://$", { "Ref": "AdvertisedIPAddress" }, "_ipv4:2380\n",
            "    listen-client-urls: http://0.0.0.0:2379,http://0.0.0.0:4001\n",
            "    listen-peer-urls: http://$", { "Ref": "AdvertisedIPAddress" }, "_ipv4:2380\n",
            "  units:\n",
            "    - name: etcd2.service\n",
            "      command: start\n",
            "    - name: fleet.service\n",
            "      command: start\n"
            ] ]
          }
        }
      }
    }
  }
}
