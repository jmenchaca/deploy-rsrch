# **DevOps Concepts**

This is my understanding acquired from online readings in variety of sources.  I hear a lot of these concepts at conventions, such as PuppetConf or DockerCon.  This is by no means a complete list.

Joaquin Menchaca (Nov 27, 2016)

## **Patterns of Configuration**

* **Pets** - each server managed uniquely with loving care
    * **Divergence** - configuration state diverges
       * **Snowflake** - each server is unique
* **Cattle** - servers are configured en-masse
    * **Convergence** - configuration state converges
       * **Snowflake** - each server is unique
    * **Congruence** - configuration state is identical
       * **Pheonix** - server is clone, previous clone destroyed


## **Process vs Automation**

In the early days of *pets* and *divergence* configuration patterns c. 1990s, _planning_ and a strict _change control_ process was used:

* **Planning** - upfront planning (documentation) use to carefully plan out infrastructure, services, or softare, e.g. [Waterfall](http://www.softwaretestinghelp.com/what-is-sdlc-waterfall-model/) SDLC, [PMBOK](https://www.pmi.org/pmbok-guide-standards/foundational/pmbok) for traditional project management, [BABOK](https://www.iiba.org/babok-guide.aspx) for traditional business analysis
* **Change Control** process to document change and communicate change to stake holders to avoid outages, sometimes through strict change review process, or change review board. See [ITIL](https://en.wikipedia.org/wiki/ITIL).

In iterative SDLC with [Agile](http://agilemethodology.org/), [Lean](https://leankit.com/learn/lean/what-is-lean/), and [Kanban](https://leankit.com/learn/kanban/what-is-kanban/) principals, upfront planning is less important and documentation is the code; change is controlled through source code control, e.g. `git`, with a peer review process.  This gives rise to new patterns.

 * **Infrastructure as Code** - server or infrastructure configuration is documented in configuration source code (typically checked into source code repository)
 * **Immutable Production** - servers created from image with state configured at deploy time or fetched from external source (service discovery).
     * **Golden Image** - earlier pattern where images are built statically; this pattern no longer applicable with cached layered images

## **Promise Theory**

* Guiding Behavior:
    * **Promise** - agents in a system should have autonomy of control, and cannot be coerced or forced into a behavior.
    * **Obligation** - deterministic command that causes a proposed outcome
* Implementations
    * Obligation:
        * Remote Execution, Deploy, Agentless Change Config: [Capistrano](http://capistranorb.com/) [Fabric](http://www.fabfile.org/), [Mina](http://nadarei.co/mina/), [Ansible](https://www.ansible.com/), [MCollective](https://docs.puppet.com/mcollective/), [SaltStack](https://saltstack.com/), [Terraform](https://www.terraform.io/)
        * Orchestration: [Kubernetes](http://kubernetes.io/),  [Mesos](http://mesos.apache.org/), [Fleet](https://coreos.com/fleet/), [Swarm](https://www.docker.com/products/docker-swarm)
    * Promise:
        * Agent Based Change Config: [CFEngine](https://cfengine.com/), [Puppet](https://puppet.com/). [Chef](https://www.chef.io/chef/)
        * Service Discovery:  [Zookeeper](http://zookeeper.apache.org/), [Etcd](https://coreos.com/etcd/) [Consul](https://www.consul.io/)
        * Choreography: [Habitat](https://www.habitat.sh/), [InfraKit](https://blog.docker.com/2016/10/introducing-infrakit-an-open-source-toolkit-for-declarative-infrastructure/)

The above is not an exhaustive list, just examples of tools I have come across.  Note that some of these tools may use both obligation and change configuration, e.g. Chef is trigged with `knife` (_obligation_), but chef client can pull (_promise_).  Also, container orchestration platforms will orchestrate container deployment (_obligration_), but container based application may self-configure through service discovery (_promise_).

## **Links**

* **Divergence, Convergence, Congruence**
    * [Why Order Matters: Turing Equivalence in Automated Systems Administration](http://www.infrastructures.org/papers/turing/turing.html) by Steve Traugott (Nov 2002)
    * [Divergent, Convergent, and Congruent Infrastructures](https://constructolution.wordpress.com/2012/07/08/divergent-convergent-and-congruent-infrastructures/) By Paul Guth (Jul 2012)
* **Pets vs. Cattle**
    * [Scaling SQL Server 2012](http://www.sqlpass.org/EventDownload.aspx?suid=1902) by Glenn Berry (Oct 2011)
    * [CERN Data Centre Evolution](http://www.slideshare.net/gmccance/cern-data-centre-evolution) by Gavin McCane (Nov 2012)
    * [Pets vs. Cattle ](https://blog.engineyard.com/2014/pets-vs-cattle) by Noah Slater (Feb 2014)
    * [The History of Pets vs Cattle and How to Use the Analogy Properly](http://cloudscaling.com/blog/cloud-computing/the-history-of-pets-vs-cattle/) by Randy Bias (Sep 2016)
* **Snowflake vs. Pheonix Server Pattern**
    * [SnowflakeServer](http://martinfowler.com/bliki/SnowflakeServer.html) by Martin Fowler (Jul 2012)
    * [PhoenixServer](http://martinfowler.com/bliki/PhoenixServer.html) by Martin Fowler (Jul 2012)
    * [DevOps is Ruining My Craft](http://tatiyants.com/devops-is-ruining-my-craft/) by Alex Tatiyants (Feb 2012)
    * [Moving to the Phoenix Server Pattern - Part 1: Introduction](https://www.thoughtworks.com/insights/blog/moving-to-phoenix-server-pattern-introduction) (Jul 2015)
    * [Moving to the Phoenix Server Pattern - Part 2: Things to Consider](https://www.thoughtworks.com/insights/blog/moving-to-phoenix-server-pattern-things-to-consider) (Sep 2015)
    * [Moving to the Phoenix Server Pattern Part 3: Evolving a Recovery Strategy](https://www.thoughtworks.com/insights/blog/moving-using-phoenix-pattern-part-3-evolving-recovery-strategy) (Feb 2016)
    * **Visible Ops Book**
        * [The Visible Ops Handbook: Implementing ITIL in 4 Practical and Auditable Steps](https://www.amazon.com/Visible-Ops-Handbook-Implementing-Practical/dp/0975568612) by Gene Kim (Jun 2005)
        * [Visible Ops WikiSummary](http://www.wikisummaries.org/wiki/Visible_Ops)
        * [Visible Ops Phase Three: Create A Repeatable Build Library](https://rnelson0.com/2015/06/04/visible-ops-phase-three-create-a-repeatable-build-library/) by Rob Nelson (Jun 2015)
* **Promise Theory**
    * [Promise Theory](https://en.wikipedia.org/wiki/Promise_theory) Wikiepedia entry
* **Scheduler Comparisons**
    * [Comparison of Container Schedulers](https://medium.com/@ArmandGrillet/comparison-of-container-schedulers-c427f4f7421#.8prpgv5vb)
    * [Nomad on Hacker News](https://news.ycombinator.com/item?id=10291777)
* **Concurrency Control**
    * [Concurrency Control Mechanisms](https://www.ibm.com/support/knowledgecenter/SSPK3V_7.0.0/com.ibm.swg.im.soliddb.sql.doc/doc/pessimistic.vs.optimistic.concurrency.control.html)
    * [Omega:  flexible,  scalable  schedulers  for  large  compute  clusters](http://static.googleusercontent.com/media/research.google.com/en//pubs/archive/41684.pdf)
