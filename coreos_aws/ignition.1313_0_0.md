## Ignition config

Container Linux allows you to configure machine parameters, configure networking, launch systemd units on startup, and more via Ignition. Head over to the [docs to learn about the supported features][ignition-docs].

You can provide a raw Ignition config to Container Linux via the Amazon web console or [via the EC2 API][ec2-user-data].

As an example, this config will configure and start etcd:

**Container Linux Config**
This is the human-readable config file. This should not be immediately passed to Container Linux. [Learn more](https://coreos.com/os/docs/latest/overview-of-ct.html).

```yaml
systemd:
  units:
    - name: etcd2.service
      enable: true
      dropins:
        - name: metadata.conf
          contents: |
            [Unit]
            Requires=coreos-metadata.service
            After=coreos-metadata.service

            [Service]
            EnvironmentFile=/run/metadata/coreos
            ExecStart=
            ExecStart=/usr/bin/etcd2 \
                --advertise-client-urls=http://${COREOS_EC2_IPV4_LOCAL}:2379 \
                --initial-advertise-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \
                --listen-client-urls=http://0.0.0.0:2379 \
                --listen-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \
                --discovery=https://discovery.etcd.io/<token>
```
**Ignition Config**

This is the raw machine configuration, which is not intended for editing. [Learn more](https://coreos.com/os/docs/latest/overview-of-ct.html). Validate the config [here](https://coreos.com/validate/).
```json
{
  "ignition": {
    "version": "2.0.0",
    "config": {}
  },
  "storage": {},
  "systemd": {
    "units": [
      {
        "name": "etcd2.service",
        "enable": true,
        "dropins": [
          {
            "name": "metadata.conf",
            "contents": "[Unit]\nRequires=coreos-metadata.service\nAfter=coreos-metadata.service\n\n[Service]\nEnvironmentFile=/run/metadata/coreos\nExecStart=\nExecStart=/usr/bin/etcd2 \\n    --advertise-client-urls=http://${COREOS_EC2_IPV4_LOCAL}:2379 \\n    --initial-advertise-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \\n    --listen-client-urls=http://0.0.0.0:2379 \\n    --listen-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \\n    --discovery=https://discovery.etcd.io/\u003ctoken\u003e"
          }
        ]
      }
    ]
  },
  "networkd": {},
  "passwd": {}
}
```


[ec2-user-data]: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
[ignition-docs]: https://coreos.com/ignition/docs/latest

### Instance storage

Ephemeral disks and additional EBS volumes attached to instances can be mounted with a `.mount` unit. Amazon's block storage devices are attached differently [depending on the instance type](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html#InstanceStoreDeviceNames). Here's the Ignition config to format and mount the first ephemeral disk, `xvdb`, on most instance types:

**Contianer Linux Config**

 This is the human-readable config file. This should not be immediately passed to Container Linux. [Learn more](https://coreos.com/os/docs/latest/overview-of-ct.html).

```yaml
storage:
  filesystems:
    - mount:
        device: /dev/xvdb
        format: ext4
        create:

systemd:
  units:
    - name: media-ephemeral.mount
      enable: true
      contents: |
        [Mount]
        What=/dev/xvdb
        Where=/media/ephemeral
        Type=ext4

        [Install]
        RequiredBy=local-fs.target
```

**Ignition Config**
 This is the raw machine configuration, which is not intended for editing. [Learn more](https://coreos.com/os/docs/latest/overview-of-ct.html). Validate the config [here](https://coreos.com/validate/).

```json
{
  "ignition": {
    "version": "2.0.0",
    "config": {}
  },
  "storage": {
    "filesystems": [
      {
        "mount": {
          "device": "/dev/xvdb",
          "format": "ext4"
        }
      }
    ]
  },
  "systemd": {
    "units": [
      {
        "name": "media-ephemeral.mount",
        "enable": true,
        "contents": "[Mount]\nWhat=/dev/xvdb\nWhere=/media/ephemeral\nType=ext4\n\n[Install]\nRequiredBy=local-fs.target"
      }
    ]
  },
  "networkd": {},
  "passwd": {}
}
```


For more information about mounting storage, Amazon's [own documentation](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html) is the best source. You can also read about [mounting storage on Container Linux](mounting-storage.md).

### Adding more machines

To add more instances to the cluster, just launch more with the same Ignition config, the appropriate security group and the AMI for that region. New instances will join the cluster regardless of region if the security groups are configured correctly.

## SSH to your instances

Container Linux is set up to be a little more secure than other cloud images. By default, it uses the `core` user instead of `root` and doesn't use a password for authentication. You'll need to add an SSH key(s) via the AWS console or add keys/passwords via your Ignition config in order to log in.

To connect to an instance after it's created, run:

```sh
ssh core@<ip address>
```

Optionally, you may want to [configure your ssh-agent](https://github.com/coreos/fleet/blob/master/Documentation/using-the-client.md#remote-fleet-access) to more easily run [fleet commands](../fleet/launching-containers-fleet.md).

## Multiple clusters
If you would like to create multiple clusters you will need to change the "Stack Name". You can find the direct [template file on S3](https://s3.amazonaws.com/coreos.com/dist/aws/coreos-stable-hvm.template).

## Manual setup

TL;DR: launch three instances of [ami-6338c275](https://console.aws.amazon.com/ec2/home?region=us-east-1#launchAmi=ami-6338c275) in **us-east-1** with a security group that has open port `22`, `2379`, `2380`, `4001`, and `7001` and the same "User Data" of each host. SSH uses the `core` user and you have [etcd][etcd-docs] and [Docker][docker-docs] to play with.

### Creating the security group

You need open port 2379, 2380, 7001 and 4001 between servers in the `etcd` cluster. Step by step instructions below.

_This step is only needed once_

First we need to create a security group to allow Container Linux instances to communicate with one another.

1. Go to the [security group][sg] page in the EC2 console.
2. Click "Create Security Group"
    * Name: coreos-testing
    * Description: Container Linux instances
    * VPC: No VPC
    * Click: "Yes, Create"
3. In the details of the security group, click the `Inbound` tab
4. First, create a security group rule for SSH
    * Create a new rule: `SSH`
    * Source: 0.0.0.0/0
    * Click: "Add Rule"
5. Add two security group rules for etcd communication
    * Create a new rule: `Custom TCP rule`
    * Port range: 2379
    * Source: type "coreos-testing" until your security group auto-completes. Should be something like "sg-8d4feabc"
    * Click: "Add Rule"
    * Repeat this process for port range 2380, 4001 and 7001 as well
6. Click "Apply Rule Changes"

[sg]: https://console.aws.amazon.com/ec2/home?region=us-east-1#s=SecurityGroups

### Launching a test cluster

We will be launching three instances, with a few parameters in the User Data, and selecting our security group.

1. Open the quick launch wizard to boot ami-6338c275.
* On the second page of the wizard, launch 3 servers to test our clustering
    * Number of instances: 3
    * Click "Continue"
* Next, we need to specify a discovery URL, which contains a unique token that allows us to find other hosts in our cluster. If you're launching your first machine, generate one at https://discovery.etcd.io/new?size=3, configure the `?size=` to your initial cluster size and add it to the metadata. You should re-use this key for each machine in the cluster.
*  
```yaml
systemd:
  units:
    - name: etcd2.service
      enable: true
      dropins:
        - name: cluster.conf
          # generate a new token for each unique cluster from https://discovery.etcd.io/new?size=3
          # specify the initial size of your cluster with ?size=X
          contents: |
            [Unit]
            Requires=coreos-metadata.service
            After=coreos-metadata.service

            [Service]
            EnvironmentFile=/run/metadata/coreos
            ExecStart=
            ExecStart=/usr/bin/etcd2 \
                --advertise-client-urls=http://${COREOS_EC2_IPV4_PUBLIC}:2379 \
                --initial-advertise-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \
                --listen-client-urls=http://0.0.0.0:2379 \
                --listen-peer-urls=http://${COREOS_EC2_IPV4_LOCAL}:2380 \
                --discovery=https://discovery.etcd.io/<token>
    - name: fleet.service
      enable: true
```
Back in the EC2 dashboard, paste this information verbatim into the "User Data" field.
    * Paste link into "User Data"
    * "Continue"
* Storage Configuration
   * "Continue"
* Tags
   * "Continue"
* Create Key Pair
   * Choose a key of your choice, it will be added in addition to the one in the gist.
   * "Continue"
* Choose one or more of your existing Security Groups
   * "coreos-testing" as above.
   * "Continue"
* Launch!



## Using CoreOS Container Linux

Now that you have a machine booted it is time to play around. Check out the [Container Linux Quickstart](quickstart.md) guide or dig into [more specific topics](https://coreos.com/docs).


[coreos-user]: https://groups.google.com/forum/#!forum/coreos-user
[docker-docs]: https://docs.docker.io
[etcd-docs]: https://github.com/coreos/etcd/tree/master/Documentation
[irc]: irc://irc.freenode.org:6667/#coreos
