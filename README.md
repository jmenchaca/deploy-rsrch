# **Deploy & Orchestrate Research**

## **Overview**

This is research related to my fascination with deployment, both old school and current environment with distributed scheduling and orchestration platforms.

* **change**      - change config (often used in conjunction with deploy)
* **container**   - container platforms
* **devtool**     - local dev orchestrators and other tools
* **deploy**      - classic tools for remote execution and deploying software
* **discovery**   - service discovery solutions used by these tools
* **image_build** - building base system/image
* **mesos_apps**  - mesos packaged applications of interest
* **orchestrate** - orchestration deploy tools/systems
* **resource**    - resource managers
* **routing**     - tools using for load balancing and routing
* **schedule**    - simple or distributed scheduling that may include orchestration

## **Fetch Script**

A convenience script `fetch_repos.pl` can fetch all the repos into a local `repos/subcategory` directory.

**Requirements**

* Perl 5 (*tested on 5.24*)
* Perl modules: `YAML:XS`

### **Mac OS X**

Instructions valid as of Nov, 2016.

```bash
# Install Perl latest stable
brew install perl
# Setup Environment
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5" cpan local::lib
echo 'eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"' >> ~/.bash_profile
# Install Modules in current environment
eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"
# Install Package Manager Wrapper
brew install cpanminus
# Install Perl Modules
cpanm YAML::XS
```

### **Arch Linux**

This should work for [Arch Linux](https://www.archlinux.org/) and [Apricity](https://apricityos.com/).

```bash
# Perl Package Manager
sudo pacman -S cpanminus
# Required Perl Libraries for YAML parsing
cpanm YAML::XS
# Mercurial HG tools
sudo pacman -S mercurial
```
