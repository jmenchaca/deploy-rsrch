#!/usr/bin/env perl
use strict;
use warnings;
use YAML::XS 'LoadFile';
use Cwd;

my $repodir="${\getcwd}/repos";
my %categories = %{LoadFile("repos.yml")};

foreach my $category (keys %categories) {
    chdir $repodir;    # change to root directory
    mkdir $category;   # make sub-directory category

    # get list of repositories to `git clone`
    my %repos = %{$categories{$category}};
    chdir $category;

    # `git clone` repositories
    foreach my $repo (keys %repos) {
        print "\nGit Pulling Repo \"${\getcwd}/$repo\"\n";
        if ($repos{$repo} =~ /hg@/) {
          `pushd $repo; hg pull; popd\n`;
        } else {
          `pushd $repo; git pull; popd\n`;
        }

    }
}
