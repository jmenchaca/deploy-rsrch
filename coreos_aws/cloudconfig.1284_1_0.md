## Cloud-config

Container Linux allows you to configure machine parameters, launch systemd units on startup and more via cloud-config. Jump over to the docs to learn about the supported features. Cloud-config is intended to bring up a cluster of machines into a minimal useful state and ideally shouldn't be used to configure anything that isn't standard across many hosts. Once a machine is created on EC2, the cloud-config can only be modified after it is stopped or recreated.

You can provide raw cloud-config data to Container Linux via the Amazon web console or via the EC2 API. Our CloudFormation template supports the most common cloud-config options as well.

The most common cloud-config for EC2 looks like:

```yaml
#cloud-config

coreos:
  etcd2:
    # generate a new token for each unique cluster from https://discovery.etcd.io/new?size=3
    # specify the initial size of your cluster with ?size=X
    discovery: https://discovery.etcd.io/<token>
    # multi-region and multi-cloud deployments need to use $public_ipv4
    advertise-client-urls: http://$private_ipv4:2379,http://$private_ipv4:4001
    initial-advertise-peer-urls: http://$private_ipv4:2380
    # listen on both the official ports and the legacy ports
    # legacy ports can be omitted if your application doesn't depend on them
    listen-client-urls: http://0.0.0.0:2379,http://0.0.0.0:4001
    listen-peer-urls: http://$private_ipv4:2380,http://$private_ipv4:7001
  units:
    - name: etcd2.service
      command: start
    - name: fleet.service
      command: start
```

The $private_ipv4 and $public_ipv4 substitution variables are fully supported in cloud-config on EC2.

### Instance storage

Ephemeral disks and additional EBS volumes attached to instances can be mounted with a `.mount` unit. Amazon's block storage devices are attached differently [depending on the instance type](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html#InstanceStoreDeviceNames). Here's the cloud-config to mount the first ephemeral disk, xvdb on most instance types:

```yaml
#cloud-config
coreos:
  units:
    - name: media-ephemeral.mount
      command: start
      content: |
        [Mount]
        What=/dev/xvdb
        Where=/media/ephemeral
        Type=ext3
```

For more information about mounting storage, Amazon's [own documentation](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html) is the best source. You can also read about [mounting storage on Container Linux](https://coreos.com/os/docs/1284.1.0/mounting-storage.html).

### Adding more machines

To add more instances to the cluster, just launch more with the same cloud-config, the appropriate security group and the AMI for that region. New instances will join the cluster regardless of region if the security groups are configured correctly.

## SSH to your instances

Container Linux is set up to be a little more secure than other cloud images. By default, it uses the `core` user instead of `root` and doesn't use a password for authentication. You'll need to add an SSH key(s) via the AWS console or add keys/passwords via your cloud-config in order to log in.

To connect to an instance after it's created, run:

```
ssh core@<ip address>
```

## Multiple clusters

If you would like to create multiple clusters you will need to change the "Stack Name". You can find the direct [template file on S3](https://s3.amazonaws.com/coreos.com/dist/aws/coreos-stable-hvm.template).


## Manual setup

TL;DR: launch three instances of [ami-6338c275](https://console.aws.amazon.com/ec2/home?region=us-east-1#launchAmi=ami-6338c275) in us-east-1 with a security group that has open port `22`, `2379`, `2380`, `4001`, and `7001` and the same "User Data" of each host. SSH uses the `core` user and you have [etcd](https://github.com/coreos/etcd/tree/master/Documentation) and [Docker](https://docs.docker.io/) to play with.

Creating the security group

You need open port `2379`, `2380`, `7001` and `4001` between servers in the etcd cluster. Step by step instructions below.

This step is only needed once

First we need to create a security group to allow Container Linux instances to communicate with one another.

1. Go to the [security group](https://console.aws.amazon.com/ec2/home?region=us-east-1#s=SecurityGroups) page in the EC2 console.
* Click "Create Security Group"
    * Name: coreos-testing
    * Description: Container Linux instances
    * VPC: No VPC
    * Click: "Yes, Create"
* In the details of the security group, click the `Inbound` tab
* First, create a security group rule for SSH
    * Create a new rule: `SSH`
    * Source: 0.0.0.0/0
    * Click: "Add Rule"
* Add two security group rules for etcd communication
    * Create a new rule: `Custom TCP rule`
    * Port range: 2379
    * Source: type "coreos-testing" until your security group auto-completes. Should be something like "sg-8d4feabc"
    * Click: "Add Rule"
    * Repeat this process for port range `2380`, `4001` and `7001` as well
* Click "Apply Rule Changes"

### Launching a test cluster



We will be launching three instances, with a few parameters in the User Data, and selecting our security group.

1. Open the [quick launch wizard](https://console.aws.amazon.com/ec2/home?region=us-east-1#launchAmi=ami-c96d95df) to boot ami-c96d95df.
* On the second page of the wizard, launch 3 servers to test our clustering
    * Number of instances: 3
    * Click "Continue"
* Next, we need to specify a discovery URL, which contains a unique token that allows us to find other hosts in our cluster. If you're launching your first machine, generate one at https://discovery.etcd.io/new?size=3, configure the `?size=` to your initial cluster size and add it to the metadata. You should re-use this key for each machine in the cluster.
*
    ```yaml
    #cloud-config

    coreos:
      etcd2:
        # generate a new token for each unique cluster from https://discovery.etcd.io/new?size=3
        # specify the initial size of your cluster with ?size=X
        discovery: https://discovery.etcd.io/<token>
        # multi-region and multi-cloud deployments need to use $public_ipv4
        advertise-client-urls: http://$private_ipv4:2379,http://$private_ipv4:4001
        initial-advertise-peer-urls: http://$private_ipv4:2380
        # listen on both the official ports and the legacy ports
        # legacy ports can be omitted if your application doesn't depend on them
        listen-client-urls: http://0.0.0.0:2379,http://0.0.0.0:4001
        listen-peer-urls: http://$private_ipv4:2380,http://$private_ipv4:7001
      units:
        - name: etcd2.service
          command: start
        - name: fleet.service
          command: start
     ```        
Back in the EC2 dashboard, paste this information verbatim into the "User Data" field.
    * Paste link into "User Data"
    * "Continue"
* Storage Configuration
    * "Continue"
* Tags
    * "Continue"
* Create Key Pair
    * Choose a key of your choice, it will be added in addition to the one in the gist.
    * "Continue"
* Choose one or more of your existing Security Groups
    * "coreos-testing" as above.
    * "Continue"
* Launch!
