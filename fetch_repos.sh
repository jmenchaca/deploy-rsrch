#!/usr/bin/env bash

# This should work on bash 3.x or higher

##### yaml2csv()
# Adapted from:
#    http://stackoverflow.com/questions/5014632/how-can-i-parse-a-yaml-file-from-a-linux-shell-script
#######################
function yaml2csv {
   local prefix=$2
   local sep=','
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("'$sep'")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

REPO_DIR="repos"
ROOT_DIR="${PWD}/$REPO_DIR"

# split repos.yml into comma seperated file
while IFS=, read -r -a LINE; do
  CATEGORY=${LINE[0]}  # fetch category
  TARGET_DIR="${ROOT_DIR}/${CATEGORY}"  # establish target directory path

  # create a directorh structure if needed
  [ -d $TARGET_DIR ] || mkdir $TARGET_DIR
  cd $TARGET_DIR

  # clone git repository
  IFS== REPO=(${LINE[1]}) # split on '='
  git clone ${REPO[1]} ${REPO[0]}  # git clone url dir
done <<< $(cat repos.yml | yaml2csv)
