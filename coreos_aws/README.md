# CoreOS (Container Linux) AWS Template

Template Source: https://s3.amazonaws.com/coreos.com/dist/aws/coreos-stable-hvm.template

CoreOS renamed to Container Linux, and switched from Cloud-Config in build 1281.1.0 to a new system called Ignition.
